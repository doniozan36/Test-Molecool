<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Resources\NewsCollection;
use App\Http\Requests\StoreNewsPost;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new NewsCollection(News::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewsPost $request)
    {
        //upload image
        $file = $request->file('file');
        $tujuan_upload = 'image';
        $filename = date('Ymdhis').'.'.$file->getClientOriginalExtension();
        $file->move($tujuan_upload,$filename);

        $insert = $request->except('file');
        $insert['file'] = $filename;
        if (!News::create($insert)) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            \LogActivity::addToLog('Add News.');
            return [
                'message' => 'OK',
                'code' => 200,
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\New  $new
     * @return \Illuminate\Http\Response
     */
    public function show(News $new)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\New  $new
     * @return \Illuminate\Http\Response
     */
    public function edit(News $new)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\New  $new
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $validator = Validator::make($request->all(), [
            'title'         => ['required'],
            'description'   => ['required']
        ]);

        if ($validator->fails()) {
            return $validator->getMessageBag();
        }
        if (!$news->update($request->all())) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            \LogActivity::addToLog('Update News.');
            return [
                'message' => 'OK',
                'code' => 200,
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\New  $new
     * @return \Illuminate\Http\Response
     */
    public function destroy($new)
    {
        if (News::find($new)->delete()) {
            \LogActivity::addToLog('Delete News.');
            return [
                'message' => 'OK',
                'code' => 200,
            ];
        } else {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        }
    }
}
