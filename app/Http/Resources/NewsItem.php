<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'last_name' => $this->description,
            'file' => url('image/'.$this->file),
            'created_at' => $this->created_at,
            'comment'   => $this->comment
        ];
    }
}
