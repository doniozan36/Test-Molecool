<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'comment';
    protected $fillable = ['comment','news_id'];
    protected $dates = ['deleted_at'];

    public function news(){
        return $this->belongsTo(News::class);
    }
}
