<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'news';
    protected $fillable = ['title','description','file'];
    protected $dates = ['deleted_at'];
    
    public function comment()
    {
      return $this->hasMany(Comment::class);
    }
}
